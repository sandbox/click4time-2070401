This set of modules is intended to provide Click4Time short codes.

In order to use this module you will need an account at Click4Time.com: https://book.click4time.com/signup?ref=PIN183 

Click4Time is an award winning booking solution that's feature rich and easy to use for both you and your clients.

Click4Time is ideal for any business that books time, including massage therapists, acupuncturists, counselors, salons, fitness trainers, spas, home services, financial planners, personal and business coaches, to name just a few. Our fees are:

    Free for a single Service Provider for up to 20 bookings / month
    $9.95 - Single Service Provider or Resource with up to 100 bookings / month
    $19.95 - Single Service Provider or Resource with unlimited bookings / month
    $39.95 - Multiple Service Providers or Resources (2-20) with unlimited bookings / month.

Here are just a few reasons to use Click4Time:

    Grow your business through our built-in online marketing.
    Clients can book and manage their appointments 24/7.
    Reduce no shows with email or text message appointment notifications.
    Service Providers can remotely manage their appointment and schedules.
    Service Providers can sync their appointments with their Google, iCal, iPhone and Android calendars.
    Smartphone, tablet and desktop compatible.
    Built-in business referral system to reward those who help promote your business.
    Financial and statistical reporting.
    Client management tools.
    Fast and Secure.

Give your clients the benefit of booking and managing their appointments online, anytime, anywhere.

Our 60 Day Free Trial is more than enough time to prove the benefits of our online booking system. Go to https://book.click4time.com/signup?ref=PIN183 to signup now.

Setup

1. Install it for Developers (unpacking it to your Drupal sites/all/modules directory if you're installing by hand, for example).
2. Enable all Click4Time modules in Admin menu > Modules.
3. The Click4Time modules requires the core module - Filter enabled. 
4. Configure the Filter module, make sure that all click4time modules are checked. 
5. For Filtered HTML, you need to add <script> and <img> tags into the "Allowed HTML tags" input box of the Filter settings.
6. To choose a Book Now button style, go to https://book.click4time.com, log in, if not already, and navigate to your Dashboard Marketing / Book Now Button tab.  Choose a button style and size and copy the button code from one of the View options boxes.  In the Modules / Book Now Button section, choose "Configure" and past the code into the Embedded Code box and Save.
7. To add your Embedded Calendar code, go to your Click4Time Dashboard, Marketing / Book Now Button tab. Generate the Embedded Calendar code if necessary, then copy and past the code in to the Configuration window for the Embedded Calendar module in your Drupal admin and Save. Note, there will be demo code displayed in the Book Now Button and Embedded Calendar Configuration windows in Drupal.  You must replace this code with code from your Click4Time Book Now Button tab to display your services.
8. To refresh the display of the new content on your site, go to http://yoursite.com/update.php. Further instructions on updating modules can be found at https://drupal.org/node/250790. 